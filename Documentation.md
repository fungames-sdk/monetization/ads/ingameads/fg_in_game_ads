## In-Game Ads

## Introduction

In-Game Ads are a type of Ad that can be directly integrated in your game environment to maximize revenues without interfering with the user experience.

## Integration Steps

1) **"Install"** or **"Upload"** FG InGameAds plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Remote Config Settings

If you intend to display In Game Ads placements according to a Remote Config value (ex: Adverty OR Gadsme in the same billboard), you can attach the **FGIGAdPlacementWithRemoteConfig** component to the billboard containing the ad placements and fill it with the different placements to display. 

This script use "**InGameAdsConfig**" remote value to chose which ad will be displayed and can be also used to handle backfill feature as follow :

```InGameAdsConfig = 213 (example)```

(here 0,1,2,3, etc.. refers to the config value for each network that we already use: Anzu=2, Adverty=1, Gadsme =3 etc.)

# Scripting API

> You can subscribe to these callbacks to attach custom actions to InGameAds Events:

```csharp
FGIGAds.Callbacks.Initialization;
FGIGAds.Callbacks.OnInitialized;
FGIGAds.Callbacks.OnAdDisplayed;
FGIGAds.Callbacks.OnAdLoaded;
FGIGAds.Callbacks.OnAdRequestFailed;
FGIGAds.Callbacks.OnAdImpressionValidated;
FGIGAds.Callbacks.OnAdClicked;
FGIGAds.Callbacks.OnAdCompleted;
```